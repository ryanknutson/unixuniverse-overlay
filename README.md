# UnixUniverse Gentoo Overlay

<img src="larry.svg" height="200px" width="200px">

## Installation

make sure you have `app-eselect/eselect-repository` & `dev-vcs/git` installed

run the following to add the repository:
```
eselect repository add UnixUniverse git https://gitlab.com/ryanknutson/unixuniverse-overlay.git
```

run to sync repository:
```
emaint sync --repo UnixUniverse
```